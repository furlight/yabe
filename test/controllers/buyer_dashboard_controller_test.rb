require "test_helper"

class BuyerDashboardControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get buyer_dashboard_index_url
    assert_response :success
  end
end
