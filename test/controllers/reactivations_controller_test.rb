require "test_helper"

class ReactivationsControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get reactivations_edit_url
    assert_response :success
  end

  test "should get update" do
    get reactivations_update_url
    assert_response :success
  end
end
