class SellerDashboardController < ApplicationController
  before_action :authenticate_seller!

  def index
    @products = current_user.seller.products.with_deleted
  end

  private

  def authenticate_seller!
    unless current_user&.seller?
      redirect_to root_path, alert: 'You are not authorized to access this page.'
    end
  end
end
