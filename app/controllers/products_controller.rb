class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy, :restore, :add_to_cart, :buy_now]
  before_action :authorize_seller!, only: [:edit, :update, :destroy, :restore]

  def index
    @products = Product.all
  end

  def show
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    @product.seller = current_user.seller
    if @product.save
      redirect_to @product, notice: 'Product was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @product.destroy
    redirect_to products_url, notice: 'Product was successfully deleted.'
  end

  def restore
    @product.restore
    redirect_to seller_dashboard_index_path, notice: 'Product was successfully restored.'
  end

  def add_to_cart
    cart = current_user.buyer.cart || current_user.buyer.create_cart
    cart_item = cart.cart_items.find_or_initialize_by(product: @product)
    cart_item.quantity = (cart_item.quantity || 0) + params[:quantity].to_i
    if cart_item.save
      redirect_to cart_path, notice: 'Product was successfully added to your cart.'
    else
      redirect_to @product, alert: 'There was an issue adding the product to your cart.'
    end
  end

  def buy_now
    quantity = params[:quantity].to_i
    order = current_user.buyer.orders.new(product: @product, quantity: quantity, total_price: @product.price * quantity, status: 'pending')
    if order.save
      redirect_to order_path(order), notice: 'Product was successfully purchased.'
    else
      redirect_to @product, alert: 'There was an issue with your purchase.'
    end
  end

  private

  def set_product
    @product = Product.with_deleted.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :description, :price, :stock)
  end

  def authorize_seller!
    unless current_user&.seller? && @product.seller == current_user.seller
      redirect_to products_path, alert: 'You are not authorized to perform this action.'
    end
  end
end
