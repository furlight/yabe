class ReactivationsController < ApplicationController
  def edit
    @user = User.only_deleted.find(params[:id])
  end

  def update
    @user = User.only_deleted.find(params[:id])
    if @user.update(user_params)
      @user.restore
      redirect_to login_path, notice: 'Your account has been reactivated. Please log in with your new password.'
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
