class OrdersController < ApplicationController
  before_action :authenticate_user!, only: [:show, :create, :cancel, :update_status]

  def show
    @order = current_user.buyer.orders.find(params[:id])
  end

  def create
    @cart = current_user.buyer.cart
    @cart.cart_items.each do |cart_item|
      order = current_user.buyer.orders.new(product: cart_item.product, quantity: cart_item.quantity, total_price: cart_item.quantity * cart_item.product.price, status: 'pending')
      order.save
      cart_item.product.reduce_stock(cart_item.quantity)
    end
    @cart.cart_items.destroy_all
    redirect_to buyer_dashboard_index_path, notice: 'Order was successfully placed.'
  end

  def cancel
    @order = current_user.buyer.orders.find(params[:id])
    if @order.pending?
      @order.update(status: 'cancelled')
      @order.product.increase_stock(@order.quantity)
      redirect_to buyer_dashboard_index_path, notice: 'Order was successfully cancelled.'
    else
      redirect_to buyer_dashboard_index_path, alert: 'Order cannot be cancelled.'
    end
  end

  def update_status
    @order = Order.find(params[:id])
    if @order.update(status: params[:status])
      redirect_to seller_dashboard_index_path, notice: 'Order status was successfully updated.'
    else
      redirect_to seller_dashboard_index_path, alert: 'There was an issue updating the order status.'
    end
  end

  private

  def authenticate_user!
    redirect_to login_path, alert: 'You need to sign in or sign up before continuing.' unless current_user
  end
end
