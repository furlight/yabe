class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.with_deleted.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.deleted?
        redirect_to reactivate_user_path(user.id), alert: 'Your account is deactivated. Please set a new password to reactivate your account.'
      else
        session[:user_id] = user.id
        redirect_to root_path, notice: 'Successfully logged in.'
      end
    else
      flash.now[:alert] = 'Invalid email/password combination'
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, notice: 'Successfully logged out.'
  end
end
