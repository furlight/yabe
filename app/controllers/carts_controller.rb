class CartsController < ApplicationController
  before_action :authenticate_user!

  def show
    @cart = current_user.buyer.cart
  end

  def update_item
    @cart_item = CartItem.find(params[:id])
    new_quantity = params[:cart_item][:quantity].to_i

    if new_quantity > 0
      @cart_item.update(quantity: new_quantity)
      redirect_to cart_path, notice: 'Cart item quantity was successfully updated.'
    else
      @cart_item.destroy
      redirect_to cart_path, notice: 'Cart item was successfully removed.'
    end
  end

  def destroy_item
    @cart_item = CartItem.find(params[:id])
    @cart_item.destroy
    redirect_to cart_path, notice: 'Cart item was successfully removed.'
  end

  private

  def authenticate_user!
    redirect_to login_path, alert: 'You need to sign in or sign up before continuing.' unless current_user
  end
end
