class BuyerDashboardController < ApplicationController
  before_action :authenticate_buyer!

  def index
    @orders = current_user.buyer.orders.includes(:product)
    @cart = current_user.buyer.cart || current_user.buyer.create_cart
  end

  private

  def authenticate_buyer!
    unless current_user&.buyer
      redirect_to root_path, alert: 'You are not authorized to access this page.'
    end
  end
end
