class Buyer < ApplicationRecord
  belongs_to :user
  has_many :orders
  has_one :cart, dependent: :destroy

  after_create :create_cart

  def create_cart
    Cart.create(buyer: self) unless cart.present?
  end
end
