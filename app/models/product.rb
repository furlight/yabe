class Product < ApplicationRecord
  acts_as_paranoid

  belongs_to :seller
  has_many :cart_items
  has_many :orders

  def reduce_stock(amount)
    update(stock: stock - amount)
  end

  def increase_stock(amount)
    update(stock: stock + amount)
  end
end
