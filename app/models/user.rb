class User < ApplicationRecord
  acts_as_paranoid

  has_secure_password
  has_one :buyer, dependent: :destroy
  has_one :seller, dependent: :destroy
  has_many :orders, through: :buyer

  validates :email, presence: true, uniqueness: true

  after_create :create_buyer_account

  def seller?
    !!seller
  end

  private

  def create_buyer_account
    Buyer.create(user: self)
  end
end
