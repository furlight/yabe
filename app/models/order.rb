class Order < ApplicationRecord
  belongs_to :product
  belongs_to :buyer

  STATUSES = %w[pending confirmed shipped delivered cancelled].freeze

  validates :status, inclusion: { in: STATUSES }

  before_create :set_order_number
  after_create :reduce_product_stock

  def pending?
    status == 'pending'
  end

  def confirmed?
    status == 'confirmed'
  end

  def shipped?
    status == 'shipped'
  end

  def delivered?
    status == 'delivered'
  end

  def cancelled?
    status == 'cancelled'
  end

  private

  def set_order_number
    self.order_number = SecureRandom.hex(10)
  end

  def reduce_product_stock
    product.reduce_stock(quantity)
  end
end
