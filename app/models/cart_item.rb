class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :product

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }

  def reduce_quantity(amount)
    if amount >= quantity
      destroy
    else
      update(quantity: quantity - amount)
    end
  end
end
