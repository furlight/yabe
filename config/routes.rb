Rails.application.routes.draw do
  resources :users, only: [:new, :create, :destroy]
  resources :products do
    member do
      patch :restore
      post :add_to_cart
      post :buy_now
    end
    collection do
      delete :delete_selected
    end
  end
  resources :orders, only: [:create, :show] do
    member do
      patch :cancel
      patch :update_status
    end
  end
  resource :cart, only: [:show] do
    patch 'update_item/:id', to: 'carts#update_item', as: 'update_item'
    delete 'destroy_item/:id', to: 'carts#destroy_item', as: 'destroy_item'
  end

  resources :seller_dashboard, only: [:index]
  resources :buyer_dashboard, only: [:index]

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  get 'register', to: 'users#new'
  post 'register', to: 'users#create'
  delete 'deactivate_account/:id', to: 'users#destroy', as: 'deactivate_account'
  get 'reactivate/:id', to: 'reactivations#edit', as: 'reactivate_user'
  patch 'reactivate/:id', to: 'reactivations#update'

  post 'create_order_from_cart', to: 'orders#create', as: 'create_order_from_cart'

  root 'products#index'
end
