# Projet Yabe (Ebay)

Voici le lien du git : https://gitlab.com/furlight/yabe.git

Yabe est une application web de type marketplace inspirée d'eBay. Elle permet aux utilisateurs de vendre et d'acheter des produits en ligne. L'application offre des fonctionnalités de gestion des comptes utilisateur, des produits, des commandes, et des paniers d'achat.

## Fonctionnalités

### Utilisateurs

- **Inscription et Connexion** : Les utilisateurs peuvent s'inscrire et se connecter à l'application.
- **Rôles d'Utilisateur** : Chaque utilisateur a un rôle de base de "Buyer" (Acheteur) et peut choisir d'avoir également un rôle de "Seller" (Vendeur) lors de l'inscription.
- **Gestion de Compte** : Les utilisateurs peuvent désactiver leur compte (soft delete) et le réactiver ultérieurement.

### Produits

- **CRUD Produits** : Les vendeurs peuvent créer, lire, mettre à jour et supprimer (soft delete) leurs produits.
- **Affichage des Produits** : Tous les utilisateurs peuvent visualiser les produits disponibles.
- **Stock des Produits** : Les produits affichent leur stock disponible. Les boutons d'achat et d'ajout au panier sont désactivés si le stock est à zéro.

### Paniers et Commandes

- **Panier d'Achat** : Les acheteurs peuvent ajouter des produits à leur panier, modifier les quantités ou supprimer des produits du panier.
- **Passation de Commande** : Les acheteurs peuvent passer commande directement depuis leur panier.
- **Historique des Commandes** : Les acheteurs peuvent visualiser leurs commandes passées.
- **Annulation de Commande** : Les acheteurs peuvent annuler leurs commandes tant que celles-ci sont en statut "pending".
- **Gestion des Commandes par les Vendeurs** : Les vendeurs peuvent visualiser les commandes de leurs produits et mettre à jour leur statut (confirmed, shipped, delivered).

### Dashboard

- **Dashboard Acheteur** : Permet aux acheteurs de voir leurs commandes passées et leur panier actuel.
- **Dashboard Vendeur** : Permet aux vendeurs de voir leurs produits et les commandes associées, avec la possibilité de mettre à jour le statut des commandes.

## Installation

Pour installer et exécuter ce projet en local, suivez les étapes ci-dessous :

1. Clonez le dépôt :
   ```sh
   git clone https://github.com/votre-utilisateur/yabe.git
   cd yabe
   ```

2. Installez les gemmes nécessaires :
   ```sh
   bundle install
   ```

3. Configurez la base de données :
   ```sh
   rails db:create
   rails db:migrate
   ```

4. Démarrez le serveur Rails :
   ```sh
   rails server
   ```

5. Ouvrez votre navigateur et accédez à `http://localhost:3000`.

## Configuration

### Gems Utilisées

- **devise** : Pour l'authentification des utilisateurs.
- **paranoia** : Pour les suppressions logiques (soft delete).
- **bootstrap** : Pour le style et la mise en page.
- **will_paginate** : Pour la pagination des listes de produits et de commandes.

### Modèles de Données

- **User** : Représente un utilisateur de l'application.
   - `email` : string
   - `password_digest` : string
   - Relations : `has_one :buyer`, `has_one :seller, optional: true`, `has_many :orders, through: :buyer`

- **Buyer** : Représente un acheteur.
   - `user_id` : references
   - Relations : `belongs_to :user`, `has_many :orders`

- **Seller** : Représente un vendeur.
   - `user_id` : references
   - Relations : `belongs_to :user`, `has_many :products`

- **Product** : Représente un produit.
   - `name` : string
   - `description` : text
   - `price` : decimal
   - `stock` : integer
   - `seller_id` : references
   - Relations : `belongs_to :seller`

- **Order** : Représente une commande.
   - `product_id` : references
   - `buyer_id` : references
   - `quantity` : integer
   - `total_price` : decimal
   - `status` : string
   - `order_number` : string
   - Relations : `belongs_to :product`, `belongs_to :buyer`

- **Cart** : Représente un panier.
   - `buyer_id` : references
   - Relations : `belongs_to :buyer`, `has_many :cart_items`

- **CartItem** : Représente un article dans le panier.
   - `cart_id` : references
   - `product_id` : references
   - `quantity` : integer
   - Relations : `belongs_to :cart`, `belongs_to :product`

## Licence

Ce projet est sous licence MIT. Voir le fichier [LICENSE](LICENSE) pour plus d'informations.

### Conclusion

Ce fichier `README.md` fournit une description détaillée des fonctionnalités de votre projet, des instructions d'installation, des informations sur les modèles de données et des instructions sur la manière de contribuer. Vous pouvez maintenant personnaliser ce fichier en fonction de vos besoins spécifiques et l'inclure dans votre projet Yabe.